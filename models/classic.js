import HTTP from '../utils/http'

export default  class classicModel extends HTTP {
  /**
   * 获取最新期刊
   */
  getLatest (callback) {
    this.request({
      url: 'classic/latest',
      success: (msg) => {
        callback(msg)
        this._setLatestIndex(msg.index) 
        let key = this._getKey(msg.index)
        wx.setStorageSync(key, msg)
      }
    })
  }  

  /**
   * 将最新期刊号写入缓存
   */
  _setLatestIndex (index) {
    // 同步写入缓存
    wx.setStorageSync("latestIndex", index)
  }

  /**
   * 读取缓存的index
   */
  _getLatestIndex (key) {
    let index = wx.getStorageSync(key)
    return index
  }

  /**
   * 判断是否是最旧期刊
   */
  isOldest (index) {
    return index == 1 ? true : false
  }

  /**
   * 判断是否是最新期刊
   */
  islatest(index) {
    let latestIndex = this._getLatestIndex('latestIndex')
    return index == latestIndex ? true : false
  }

  // 获取当前期刊的上一期
  // getPrevious(index, callback) {
  //   this.request({
  //     url: 'classic/'+ index + '/previous',
  //     success: (msg) => {
  //       callback(msg)
  //     }
  //   })
  // }

  // 获取当前期刊的下一期
  // getNext(index, callback) {
  //   this.request({
  //     url: 'classic/' + index + '/next',
  //     success: (msg) => {
  //       callback(msg)
  //     }
  //   })
  // }

  /**
   * 获取期刊： 当前期刊的上一期或者下一期
   */
  getClassic(index, direction, callback) {
    // 先在缓存中寻找，一旦没有，便访问服务器，然后写入缓存
    let to = direction == 'next' ? (index + 1) : (index - 1)
    let key = this._getKey(to)
    let classicData = wx.getStorageSync(key)
    if (classicData) { // 如果能找到
      callback(classicData)
    } else { // 如果不能找到
      this.request({
        url: 'classic/' + index + '/' + direction,
        success: (msg) => {
          wx.setStorageSync(this._getKey(msg.index), msg)
          callback(msg)
        }
      })
    }   
  }

  /**
   * 为classic页面制造缓存key
   */
  _getKey(index) {
    return 'classic-' + index
  }

}