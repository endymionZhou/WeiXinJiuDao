import HTTP from '../utils/http'

export default class likeModel extends HTTP {
  // 点赞或者取消赞
  onLike(url, artId, category, callback) {
    this.request({
      url,
      method: 'POST',
      data: {
        art_id: artId,
        type: category
      },
      success: (msg) => {
        callback(msg)
      }
    })
  }

  // 获取当前页面点赞消息
  getLikeStatus(category, artId, callback) {
    this.request({
      url: 'classic/'+ category + '/' + artId + '/favor',
      success: (msg) => {
        callback(msg)
      }
    })
  }
}