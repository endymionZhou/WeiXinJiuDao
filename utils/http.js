//封装wx请求
import {config} from '../config'

export default class Http {
  request (params) {
    // 如果没有定义方法类型，自动设为get
    if(!params.method) {
      params.method = 'GET'
    }
    wx.request({
      url: config.api_base_url + params.url,
      method: params.method,
      data: params.data,
      header: {
        'content-type': 'application/json',
        'appkey': config.appkey
      },
      success: (msg) => {
        let status = msg.statusCode.toString()
        // 如果状态码以二开头的话，证明成功了
        if(status.startsWith('2')) {
          //借尸还魂
          params.success(msg.data)
        }else { // 服务器异常
          wx.showToast({
            title: 'failure',
            icon: 'none',
            duration: 2000
          })   
        }
      },
      fail: (err) => { // API调用失败
        wx.showToast({
          title: 'failure',
          icon: 'none',
          duration: 2000
        })
      }
    })
  }
} 