// components/classic/movie/index.js
import {classicBh} from '../behavior'

Component({
  /**
   * 注册行为
   */
  behaviors: [classicBh],

  /**
   * 组件的属性列表
   */
  properties: {},

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})
