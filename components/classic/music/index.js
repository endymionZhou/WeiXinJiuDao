// components/classic/music/index.js
import {classicBh} from '../behavior'

const sister = wx.getBackgroundAudioManager()

Component({
  /**
   * 注册行为
   */
  behaviors: [classicBh],

  /**
   * 组件的属性列表
   */
  properties: {
    musicSrc: String
  },

  /**
   * 组件的初始数据
   */
  data: {
    imagesSrc: { //播放器图片
      play: 'images/player@playing.png',// 暂停图
      pause: 'images/player@waitting.png'//播放按钮
    },
    isPlaying: false
  },

  detached: function (event) {
    sister.stop()
  },

  /**
   * 组件的方法列表
   */
  methods: {
    onClickPlayer(event) {
      if (!this.data.isPlaying) {
        this.setData({
          isPlaying: true
        })
        sister.src = this.properties.musicSrc
      } else {
        this.setData({
          isPlaying: false
        })
        sister.pause()
      } 
    },
    processingChange() {
      if (sister.paused) {
        this.setData({
          isPlaying: false
        })
      }
    }
  }
})
