// 定义classc页面中组件的共同行为
// 这是微信小程序中解决组件复用的方式

let classicBh = Behavior({
  properties: {
    img: String,
    content: String
  },
  data: {

  },
  methods: {

  },
  attached: function () {

  }
})

export {classicBh}