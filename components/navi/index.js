// components/navi/index.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    title: String,
    isLatest: Boolean,
    isOldest: Boolean
  },

  /**
   * 组件的初始数据
   */
  data: {
    imagesSrc: {
      left: 'images/triangle@left.png',
      right: 'images/triangle@right.png',
      dissLeft: 'images/triangle.dis@left.png',
      dissRight: 'images/triangle.dis@right.png'
    }
  },

  /**
   * 组件的方法列表
   */
  methods: {
    onLeft: function () {
      //如果是不是最新的期刊，那么就可以触发向左事件
      if (!this.properties.isLatest) {
        this.triggerEvent('onLeft', {}, {})
      }  
    },
    onRight: function () {
      //如果不是最旧的期刊，那么就可以触发向右事件
      if (!this.properties.isOldest) {
        this.triggerEvent('onRight', {}, {})
      }      
    }
  }
})
