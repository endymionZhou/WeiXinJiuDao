// components/like/index.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    count: {
      type: Number,
      value: 9
    },
    like: {
      type: Boolean,
      value: false
    }
  },

  /**
   * 组件的初始数据
   */
  data: { // 私有的
    likePic: "images/like.png",
    disslikePic: "images/like@dis.png"
  },

  /**
   * 组件的方法列表
   */
  methods: {
    clickHeart: function (event) {
      let like = this.properties.like
      let count = this.properties.count
      count = like?count-1:count+1
      this.setData({
        count: count,
        like:!like
      })
      //自定义事件
      let attitude = this.properties.like?'like':'cancel'
      //三个参数：事件名字、自定义属性、不可修改的自带属性
      this.triggerEvent('onHeart', {
        attitude
      }, {})
    }
  }
})
