// components/episode/index.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    index: {
      type: String,
      //观察index是否改变，如果改变就更新data里面的值
      observer: function (newVal, oldVal, changedPath) {
        let val = newVal > 10 ? newVal : '0' + newVal
        this.setData({
          _index: val
        })
      }
    }
  },
  /**
   * 组件的初始数据
   */
  data: {
    months: [
      '壹', '贰', '叁', '肆', '伍', '陆', '柒', '捌', '玖', '拾', '拾壹', '拾贰'
    ],
    month: '',
    year: '',
    _index: ''
  },
 
  /**
   * 组件的方法列表
   */
  methods: {

  },
  /**
   * 组件的挂载之前调用
   */
  attached: function () {
    let date = new Date()
    let year = date.getFullYear()
    let month = date.getMonth() // 用函数得到的月份比真实的月份要少一

    this.setData({
      year,
      month: this.data.months[month] // 用数组就可以不用把得到的月份加一便可以得到正确的月份
    })
  }
})
